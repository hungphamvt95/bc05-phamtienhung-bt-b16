function timSoNguyenDuongNN() {
  var tong = 0;
  for (i = 1; tong <= 10000; i++) {
    tong = tong + i;
  }
  document.getElementById("result1").innerHTML = `
  Số nguyên nhỏ nhất thoả yêu cầu đề bài là: ${i - 1}`;
}

function tinhTong() {
  var x = document.getElementById("txt-so-x").value * 1;
  var n = document.getElementById("txt-so-n").value * 1;
  var tong = 0;
  for (i = 1; i <= n; i++) {
    tong = tong + Math.pow(x, i);
  }
  document.getElementById("result2").innerHTML = `Kết quả là: ${tong}`;
}

function tinhGiaiThua() {
  var n = document.getElementById("txt-so-n2").value * 1;
  var giaiThua = 1;
  for (i = 1; i <= n; i++) {
    giaiThua = giaiThua * i;
  }
  document.getElementById("result3").innerHTML = `Kết quả: ${n}! = ${giaiThua}`;
}

function inTheDiv() {
  var dsTheDiv = document.getElementById("dsTheDiv");
  dsTheDiv.innerHTML = "";
  for (let i = 0; i <= 10; i++) {
    if (i % 2 == 0) {
      var theDivChan = document.createElement("div");
      theDivChan.style.background = "red";
      theDivChan.innerHTML = `Div chẵn ${i}`;
      dsTheDiv.appendChild(theDivChan);
    } else {
      var theDivLe = document.createElement("div");
      theDivLe.innerHTML = `Div lẻ ${i}`;
      theDivLe.style.background = "blue";
      theDivLe.style.color = "white";
      dsTheDiv.appendChild(theDivLe);
    }
  }
}
